package es

import (
	"context"
	"fmt"
	"github.com/olivere/elastic/v7"
	"strings"
)

//初始化es 准备接受kafka那面发来的数据

var (
	client *elastic.Client
	//es     = make(ch chan )  可以优化
)

func Init(address string) (err error) {
	if strings.HasPrefix(address, "hhttp://") {
		address = "http://" + address
	}
	client, err = elastic.NewClient(elastic.SetURL(address))
	if err != nil {
		panic(err)
	}
	fmt.Println("connect to es access", client)
	return
}

//SendToES 往es中发送数据
func SendToES(indexStr string, data string) (err error) {
	put1, err := client.Index().Index(indexStr).BodyString(data).Do(context.Background())
	if err != nil {
		return err
	}
	fmt.Printf("index student %s to index %s ,type %s\n", put1.Id, put1.Index, put1.Type)
	return err
}
