module gitee.com/hard-dev/log_-transfer/kafka

go 1.16

require (
	gitee.com/hard-dev/log_-transfer/es v0.0.0-20210906123932-fdc28ae72f49
	github.com/Shopify/sarama v1.29.1
)
